import express from 'express';
import mongoose from 'mongoose';
import {register, login, getMe} from './controllers/UserController.js'
import {create, getAll, getOne, remove, update} from './controllers/PostController.js'
import { registerValidation, loginValidation, postCreateValidation } from './validations/auth.js';
import checkUser from './utils/checkUser.js';
import multer from 'multer';
import handleValidationErrors from './utils/handleValidationErrors.js';


mongoose.connect('mongodb+srv://vasy:vasy@cluster0.hi5zlyx.mongodb.net/blog?retryWrites=true&w=majority&appName=Cluster0')
.then(() => console.log('bd ok'))
.catch((err) =>console.log('bd error', err));


const app = express();

const storage = multer.diskStorage({
    destination: (_,file, cb) => {cb(null, 'uploads');},
    filename: (_, file, cb) => {cb(null, file.originalname);},
});

const upload = multer({storage});

app.use(express.json());
app.use('/uploads', express.static('uploads'));

app.post('/auth/register', registerValidation, handleValidationErrors, register);
app.post('/auth/login', loginValidation, handleValidationErrors, login);
app.get('/auth/me', checkUser, getMe);

app.post('/upload', checkUser, upload.single('image'),(req, res)=>{
    res.json({
        url: `/uploads/${req.file.originalname}`,
    })
})
app.post('/posts', checkUser, postCreateValidation, create);
app.get('/posts/:id', getOne);
app.get('/posts', getAll);
app.delete('/posts/:id', checkUser, remove);
app.patch('/posts/:id',checkUser, postCreateValidation, update);


app.listen(4444, (err) => {
    if(err){
        return console.log(err);
    }

    console.log('server ok');
});