import PostModel from '../models/Post.js'

export const create = async (req, res) => {
    try{
        const doc = new PostModel({
            title: req.body.title,
            text: req.body.text,
            imageUrl: req.body.imageUrl,
            tegs: req.body.tegs,
            user: req.userId,
        });
        
        const post = await doc.save();

        res.json(post)
    }

    catch(err){
        console.log(err);
        res.status(500).json({
            message: 'failed to create article',
        });
    }
};

export const getAll = async (req, res) => {
    try{
        const posts = await PostModel.find().populate('user').exec();// popular and exec подключает базу с user
        res.json(posts);
    }
    catch(err){
        console.log(err);
        res.status(500).json({
            message: 'failed to retrieve articles',
        });
    }
};

export const getOne = async (req, res) => {
    try{
        const postId = req.params.id;
    
        PostModel.findOneAndUpdate(
            {
                _id: postId
            },
            {
                $inc: {viewsCount: 1}
            },
            {
                returnDocument: 'after'
            })
            .then(doc => res.json(doc))
            .catch(err => (console.log(err), res.status(500).json({ message: 'article nod found' })))

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            message: 'failed to retrieve article',
        });
    }
};

export const remove = async (req, res) => {
    try{
        const postId = req.params.id;
    
        PostModel.findOneAndDelete({ _id: postId })
            .then(doc => { doc ? res.json({succes: true}): res.json({message: 'article not found'})})
            .catch(err => (console.log(err), res.status(500).json({ message: 'failed to delete article' })))

    }
    catch(err){
        console.log(err);
        res.status(500).json({
            message: 'failed to remove article',
        });
    }
};

export const update = async (req, res) => {
    try{
        const postId = req.params.id;
    
        await PostModel.updateOne(
            { 
                _id: postId
            },
            {
                title: req.body.title,
                text: req.body.text,
                imageUrl: req.body.imageUrl,
                tegs: req.body.tegs,
                user: req.userId,
            }
        );

        res.json({succes: true});
           
    }
    catch(err){
        console.log(err);
        res.status(500).json({
            message: 'failed to update article',
        });
    }
};

