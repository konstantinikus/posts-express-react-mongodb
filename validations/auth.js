import {body} from 'express-validator';

export const loginValidation = [
    body('email', 'invalid email format').isEmail(),
    body('password', 'password must by at least 5 characters').isLength({min:5}),
];

export const registerValidation = [
    body('email', 'invalid email format').isEmail(),
    body('password', 'password must by at least 5 characters').isLength({min:5}),
    body('fullName', 'enter your name').isLength({min:2}),
    body('avatarUrl', 'wrong link format').optional().isURL(),
];

export const postCreateValidation = [
    body('title', 'enter article title').isLength({min:2}).isString(),
    body('text', 'enter article text').isLength({min:10}).isString(),
    body('tags', 'wrong format').optional().isString(),
    body('imageUrl', 'invalid link format').optional().isString(),
];